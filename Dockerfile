FROM php:7-fpm-alpine

ENV USER_NAME=kratos
ENV USER_HOME=/home/$USER_NAME
ENV APP=/app

# Main PHP dependencies
RUN set -x \
    && apk add --no-cache libxml2-dev m4 libbz2 perl autoconf dpkg-dev dpkg file \
        libgcc libstdc++ binutils-libs binutils gmp isl libgomp libatomic mpfr3 \
        mpc1 gcc musl-dev libc-dev g++ re2c 
RUN set -x \
    && apk add --no-cache --virtual .php-deps make \
    && apk add --no-cache --virtual .build-deps $PHPIZE_DEPS zlib-dev icu icu-dev g++  \
    && apk add --no-cache openssl-dev libxml2-dev libxml2-dev libxslt-dev \
        mysql-client curl aspell-en aspell-de sqlite-dev curl-dev
        
# PHP core extensions
RUN set -x \
    # Install extensions
    && docker-php-ext-install \
        soap ftp xsl bcmath calendar ctype dba dom zip session json \
        hash sockets pdo mbstring tokenizer pdo_mysql pdo_sqlite \
        intl mysqli curl exif fileinfo

# RUN docker-php-ext-install xml xmlrpc xmlwriter simplexml
# RUN docker-php-ext-install mcrypt
# RUN docker-php-ext-install gd
# RUN docker-php-ext-install gettext
# RUN docker-php-ext-install iconv
# RUN docker-php-ext-install interbase
# RUN docker-php-ext-install pdo_firebird
# RUN docker-php-ext-install opcache
# RUN docker-php-ext-install pcntl
# RUN docker-php-ext-install phar
# RUN docker-php-ext-install posix
# RUN docker-php-ext-install pspell
# RUN docker-php-ext-install recode
# RUN docker-php-ext-install shmop
# RUN docker-php-ext-install snmp
# RUN docker-php-ext-install sysvmsg
# RUN docker-php-ext-install sysvsem
# RUN docker-php-ext-install sysvshm
# RUN docker-php-ext-install tidy
# RUN docker-php-ext-install wddx

# Install utils
RUN set -x \
    && apk add --no-cache git tar zip unzip bash make wget nano util-linux

# Create user
RUN adduser -D -u 1000 -s /bin/bash $USER_NAME

# Install Composer
RUN set -x \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
    && php -r "unlink('composer-setup.php');"
 
# Create app dir
RUN set -x \
    && mkdir -p $APP \
    && chown -R $USER_NAME.$USER_NAME $APP

# Bash setup
RUN set -x \
    # Colorful root bash
    && echo 'export PS1="\e[1m\e[91mGodOfWar\e[39m:\e[96m\w\e[0m# "' > /root/.bashrc \
    # Colorful limited user bash
    && echo 'export PS1="\e[1m\e[32m\\u\e[39m@\e[34masgard\e[39m:\e[96m\w\e[0m$ "' > $USER_HOME/.bashrc \
    # Add composer to path
    && echo 'export PATH=$PATH:/app/vendor/bin' >> $USER_HOME/.bashrc 

ADD ./*.sh /
ENTRYPOINT ["/entrypoint.sh"]

USER root

WORKDIR $APP
